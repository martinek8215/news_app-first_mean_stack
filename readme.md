# Sample News Application with MEAN stack #

This is a sample CRUD application built with with MongoDB, Express, Angular, Node.JS.

The application allows you to:

1. Create new posts
2. View all posts ordered by upvotes
3. Add comments about a given post
4. View comments for a given post
5. Upvote posts and comments
6. Authentication by npm passport local strategy.